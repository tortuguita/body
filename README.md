# Tortuguita

Tortuguita is open source, hackable, easy to build tracked robot, powered by Raspberry Pi Zero.

This project contains instructions how to manufacture and assemble your Tortuguita.
That means: all *.stl files, instructions and some additional stuff.

## License

All creative work in this project (including STL files, images and instructions) 
is licensed under [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) (Creative commons-Attribution-ShareAlike) license. 

You can freely use, copy, distribute, study and modify those files, 
use it in your commerce project(s), but you must keep it under the same license, 
and attribute the original source (this repository and/or Tortuguita project).  

And also, everything comes with no warranty at all.


## Structure: important files and directories

[Complete instructions in one file](instructions.md)    
[Makefile](Makefile) - some common actions are defined here, see note below  
[stl](stl/) - Subdirectory with stl files for 3d print  

## Makefile

*Note: Makefile is Linux stuff. If you are on Windows and not familiar with make, 
those actions will be most probably not available to you. 
But do not panic, they are for convenience and you can easily build your Tortuguita 
without using make.*

```make pdf``` generates a nicely formatted PDF from instructions. You'll need [pandoc](https://pandoc.org/) for that. 

Type ```make``` to see other available actions (if there are any).

