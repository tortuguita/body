help:
	@echo "Tortuguita makefile. Available actions:"
	@echo ""
	@echo "  pdf            Generate tortuguita_instructions.pdf"
	@echo ""

pdf:
	pandoc instructions.md -o public/tortuguita_instructions.pdf