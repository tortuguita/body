---
documentclass: extarticle
colorlinks: true
fontsize: 13pt
geometry:
- top=20mm
- left=20mm
- right=20mm
- bottom=20mm
---

# Tortuguita

This manual describes how to manufacture and assemble your Tortuguita.

Tortuguita consist of eight main assembly units: chassis, idlers, sprockets, road wheels, tracks, motors, powerset and brain.
Below are instructions how to build those units. Build is relatively easy, using common tools. 

There is a lot of parts to be printed. STL files are in /stl subdirectory.    
All parts are printed from PET-G with 100% infill, and with proper orientation they need no brim and no supports.
Layer thickness 0.2 mm is just right.

Note: if you want to save time printing and just assemble parts together,
simply order some of the kits in [Tortuguita shop](https://tortuguita.tech). 

Enjoy your project!  

# Chassis

Chassis is a body of your Tortuguita, holding everything together.

### Material bill

| Count |Name |
|---|:---|
|     1|Chassis bottom cover |
|     1|Chassis front bumper |
|     1|Chassis rear bumper |
|     2|Chassis road arm holder |
|     2|Chassis side |
|     1|Chassis top plate |
|     6|Wood screw 3,5 x 12 mm |
|     2|Wood screw 3,5 x 20 mm |
|    18|Wood screw 3,5 x 25 mm |

## Instructions

- Put aside bottom cover and respective (12 mm) screws. Bottom cover will be mounted as a last step of Tortuguita assembly.
- Screw everything together, as shown on picture. Bumpers are different (rear bumper has symmetrical holes on sides). Two 20 mm screws are for attaching rear bumper to plastic sprocket holder. Note the orientation of road arm holders.

# Idlers

Idlers are front (not powered) wheels. Idlers are identical for left and right side.

### Material bill

| Count |Name |
|---|:---|
|     2|3D printed idler wheel holder (stl/idler_holder.stl)|
|     2|3D printed idler wheel inner part (stl/idler_inner.stl)|
|     2|3D printed idler wheel outer part (stl/idler_outer.stl)|
|     2|Bearing B608ZZ |
|     2|M5 safety nut |
|     2|M5 washer big, d=20 |
|     2|Screw M5x50, part thread, hexa head |
|     6|Wood screw 3 x 20 mm |

## Instructions

- Put the bearing into the proper place between idler outer and inner part. 
- If it's loose, apply a small piece of stick tape to the bearing rim. Bearing should fit tightly.
- Screw idler together using wood screws.
- Loosely put together idler axis: M5 screw, idler wheel holder, washer and safety nut. Do not tighten the nut yet.

![](img/idler-1.jpg){ width=300px }
![](img/idler-2.jpg){ width=300px }

![](img/idler-3.jpg){ width=300px }
![](img/idler-4.jpg){ width=300px }

# Sprockets

Sprockets are rear (powered) wheels. They are different for left and right side.

### Material bill

| Count |Name |
|---|:---|
|     2|3D printed sprocket drum (stl/sprocket_wheel_drum.stl)|
|     1|3D printed sprocket holder left - inner part (stl/sprocket_holder_left_inner.stl)|
|     1|3D printed sprocket holder left - outer part (stl/sprocket_holder_left_outer.stl)|
|     1|3D printed sprocket holder right - inner part (stl/sprocket_holder_right_inner.stl)|
|     1|3D printed sprocket holder right - outer part (stl/sprocket_holder_right_outer.stl)|
|     2|3D printed sprocket inner wheel (stl/sprocket_wheel_inner.stl)|
|     2|3D printed sprocket outer wheel (stl/sprocket_wheel_outer.stl)|
|     2|Bearing B608ZZ |
|     2|M8 safety nut |
|     2|M8 standard nut |
|    16|M8 washer |
|     2|Screw M8x50, full thread, hexa head |
|    20|Wood screw 3 x 16 mm |
|     2|Wood screw 3,5 x 20 mm |

## Instructions

- Put together sprocket axis, as shown on picture. Tighten the nut really well (use vise if possible).
- Put the bearing into the proper place between sprocket holder outer and inner part. 
- If it's loose, apply a small piece of stick tape to the bearing rim. Bearing should fit tightly.
- Screw sprocket holder parts together using wood screws
- Screw sprocket inner wheel (with grove) to the drum. Grove is facing out.
- Put this thing on the axis and secure with nut.
- Screw sprocket outer wheel to the drum. Make sure that the "fingers" on the inner and outer wheel are properly aligned.

![](img/sprocket-1.jpg){ width=220px }
![](img/sprocket-2.jpg){ width=220px }
![](img/sprocket-3.jpg){ width=220px }

![](img/sprocket-4.jpg){ width=220px }
![](img/sprocket-5.jpg){ width=220px }
![](img/sprocket-6.jpg){ width=220px }

# Road wheels

Eight road wheel pairs with their respective arms (4 left and 4 right).

### Material bill

| Count |Name |
|---|:---|
|     4|3D printed left wheel arm (stl/wheel_arm_left.stl)|
|     4|3D printed right wheel arm (stl/wheel_arm_right.stl)|
|    16|3D printed road wheel (stl/road_wheel.stl)|
|     8|M4 safety nut |
|    32|M4 washer |
|     8|Screw M4x30, lentile head |
|     8|Wood screw 3,5 x 25 mm |
|     8|Worm M3x5, hexa hole |

## Instructions

- Widen small hole on the bottom side of wheel arm with a 2,7 mm diameter drill bit
- Screw worm into that hole 
- Assembly everything: for each unit you will need one bolt, 4 washers and one safety nut. Wheels must rotate freely, but not too loose. Bolt head is facing outwards. 

![](img/roadwheel-1.jpg){ width=220px }
![](img/roadwheel-2.jpg){ width=220px }
![](img/roadwheel-3.jpg){ width=220px }

# Tracks

Pair of tracks.  

### Material bill

| Count |Name |
|---|:---|
|   160|Track connector wire |
|   160|3D printed track link (stl/track_link.stl)|
|    80|3D printed track segment (stl/track_segment.stl)|

## Instructions 

- To assembly track, you will need a small flat screwdriver. 
- First assembly connectors: insert connector wire into 3D printed track link.
- Next, join pair of segments together, as shown on the picture, and bend wire ends using screwdriver.
- Repeat previous step until both tracks (40 segments each) are assembled

![](img/track-1.jpg){ width=220px }
![](img/track-2.jpg){ width=220px }
![](img/track-3.jpg){ width=220px }


# Motors

Pair of motors, each with holder and a "motor nut" mounted on axis, which acts as a coupling to sprocket axis.  
*Note: Motors are available with different gearboxes. 400 RPM is a sweet spot between sufficient strength and speed.*

### Material bill

| Count |Name |
|---|:---|
|     2|CU double cable, length 15 cm |
|     2|3D printed motor holder (stl/motor_holder.stl)|
|     2|3D printed motor nut (stl/motor_nut.stl)|
|     2|Motor with gearbox 25GA-370 12V 400 RPM ([datasheet](https://media.digikey.com/pdf/Data%20Sheets/Seeed%20Technology/114090046_Web.pdf))|
|     4|Screw M3x6, sunk head |
|     8|Wood screw 3 x 12 mm |
|     2|Worm M3x6, hexa hole |

## Instructions

- Solder cable to motor terminals
- Insert motor to holder and secure it with M3 screws
- Widen small hole on the side of "Motor nut" with a 2,7 mm diameter drill bit
- Screw worm into that hole 
- Mount entire nut to motor axis, hexa hole in nut is facing out
- Mount entire motor assembly to chassis, using wood screws. The grove in nut goes to bolt head in sprocket assembly. 
- Make sure that the track rotates freely

![](img/motor-1.jpg){ width=220px }
![](img/motor-2.jpg){ width=220px }
![](img/motor-3.jpg){ width=220px }


# PCBs

Now grab your soldering iron and solder all connectors to their PCBs, as shown on pictures.

## Connector board PCB assembly:

### Material bill

| Count |Name |
|---|:---|
|     1|PCB - connector bay |
|     1|Standard horizontal pin row 2.5 mm: M, 2 pin |
|     1|Standard pin row 2.5 mm: M, 2 pins |
|     3|Standard pin row 2.5 mm: M, 3 pins |
|     1|Standard pin row F: 4 pins, 2.5 mm |
|     1|Standard pin row F: 6 pins, 2.5 mm |


![](img/connectors-parts.jpg){ width=300px }
![](img/connectors-assembled.jpg){ width=300px }


## Powerboard PCB assembly: 

### Material bill

| Count |Name |
|---|:---|
|     1|PCB - powerboard |
|     2|Standard horizontal pin row 2.5 mm: M, 1 pin |
|     5|Standard pin row 2.5 mm: M, 2 pins |
|     3|2-pole terminal with screws, 5mm |
|     1|Standard pin row F: 4 pins, 2.5 mm |

![](img/powerboard-parts.jpg){ width=300px }
![](img/powerboard-assembled.jpg){ width=300px }


# Powerset

## Rear connector

Is used for charging or powering Tortuguita.   

### Material bill

| Count |Name |
|---|:---|
|     1|CU double cable, length 15 cm |
|     1|3D printed rear connector holder (stl/rear-connector.stl)|
|     1|Connector F 5.5/2.5 to panel |
|     2|Wood screw 3 x 10 mm |

### Instructions

- Mount connector to central hole in printed part. Tighten the nut well with flat pliers
- Solder double cable to connector terminals
- Mount entire assembly to chassis, using wood screws. Connector is facing out through the hole in rear bumper

## Battery pack

Internal battery pack. Contains control board protecting batteries from over or under charging. 
This, among other things, makes Tortuguita quite tolerant to variations in charge voltage. You can use 
pretty much any voltage source in range 11-15 V.

**WARNING:** Please note that batteries contain a lot of packed power, thus ARE dangerous. 
Especially switching polarity can cause not only frying your electronics, but even a neat little explosion. 
Before you will proceed, check out the wiring diagram of Battery control board and make sure you understand it: 

![](img/control-board-wiring.jpg){ width=300px }

### Material bill

| Count |Name |
|---|:---|
|     1|CU double cable 20 cm |
|     2|CU long inlay |
|     2|CU short inlay |
|     4|CU white cable 5 cm |
|     1|3D printed Battery holder bottom (stl/battery_holder_bottom.stl)|
|     2|3D printed Battery holder top (stl/battery_holder_top.stl)|
|     3|Battery Li-ion 18650 - 2600mAh |
|     1|Battery control board |
|     3|M3 standard nut |
|     3|Screw M3x10 |
|     4|Wood screw 3 x 10 mm |
|     4|Wood screw 3,5 x 20 mm |

### Instructions

- Solder short cables to short CU inlays, solder opposite side to Battery control board terminals B+ and B- 
- Solder short cables to long CU inlays, solder opposite side to Battery control board terminals B1 and B2 
- Solder long cables Battery control board terminals P+ and P-. Prevent other side from shorting (wrap it temprarilly with electricians tape)
- Put M3 nuts into appropriate groves in Battery bottom holder. From the outer side, screw in M3 screws, tighten well until nuts are "locked" into plastics. Then loose the screws.
- Put CU inlays into groves in Battery bottom holder.
- Put batteries in place and tighten it gently with M3 screws. Pay extreme caution to proper battery polarity.
- Place Battery control board on proper place in printed part, over the batteries. Secure with two Battery top holder parts and 4 short wood screws
- Check (with multimeter on terminals P+ and P-), if entire circuit produces any voltage. It should be something between 11-12 volts.
- Mount this entire thing into the chassis, using wood screws. Fine place is before left motor.   

## Power switch

Mains power switch.   

### Material bill

| Count |Name |
|---|:---|
|     2|CU white cable 20 cm |
|     1|3D printed power switch holder (stl/power_switch_holder.stl)|
|     2|Faston connector to cable, 4.8 mm |
|     1|Power switch - Zippy RF 12S |
|     4|Wood screw 3 x 10 mm |

### Instructions

- Power switch terminals are a little bit long, so bend it gently about 30° to the side
- Insert power switch into the hole in power switch holder, until it "clicks"
- Prepare 2 cables: crimp (with pliers) faston connector to one end of 20 cm cable. Leave other end free.
- Click on faston connectors to switch terminals
- Screw entire power switch assembly to the bottom cover, using wood screws 


# Power cable

External power cable to charge Tortuguita from 12V outlet   

### Material bill

| Count |Name |
|---|:---|
|     1|CU double cable, length 150 cm |
|     1|Connector Car 12 V male to cable |
|     1|Connector M 5.5/2.1 to cable |

### Instructions

- Solder 3.5mm jack connector to cable
- Solder 12V car connector to cable


## H-bridge and wiring

Now it is time to wire everything (except brain) together. 

### Material bill

| Count |Name |
|---|:---|
|     4|Dupont FF-FF double cable 20 cm |
|     1|Standard pin row 2.5 mm: M, 4 pins |
|     1|H-bridge motor driver |
|     1|Step-down converter |
|     8|Wood screw 3 x 10 mm |

### Instructions

- Solder 4-pin row to step down converter. 
- With solder, shorten "5V" terminals on bottom side of step down converter. This sets-up converter to produce 5V output.  
- Put entire converter into powerboard PCB
- Insert two horizontal pins on powerboard PCB into the terminal on H-Bridge driver and secure with screws
- Using wood screws, mount entire assembly to the chassis. Good place is just before right motor, H-bridge facing the motor.
- Connect wires from motors to H-bridge terminals. Right motor goes to OUT1, OUT2, left motor to PUT3, OUT4. 
- Connect wires from rear connector to powerboard terminals J3-input. Wire from inner pin goes to +, wire from outer connector shell is -. Check connection with multimeter.   
- Connect wires from battery assembly to powerboard terminals J2-battery. P+ on battery control board must go to + on powerboard, and P- to -  
- Connect wires from power switch to powerboard terminals J11-switch. Orientation does not matter.  


# Brain

### Material bill

| Count |Name |
|---|:---|
|     1|Standard pin row 2.5 mm: M, 4 pins |
|     1|3D printed connector bay - bottom part (stl/connector_bay_bottom.stl)|
|     1|3D printed connector bay - cover (stl/connector_bay_cover.stl)|
|     1|3D printed electronics holder (stl/electronics_holder.stl)|
|     4|M2 standard nut |
|     1|Micro SD card |
|     1|Raspberry Pi Zero WH |
|     1|Raspberry Pi camera |
|     1|Raspberry camera cable |
|     4|Screw M2x6 |
|     1|Step-down converter |
|    10|Wood screw 3 x 10 mm |

## Instructions 

- Connect camera and Raspberry Pi (nicknamed RPi in this manual) with a camera cable. Mind the proper cable orientation
- Put RPi on the proper place on electronics holder and secure it by melting plastic pins with tip of the soldering iron
- Secure camera with M2 screws and nuts.
- Mount connector bay to connector board PCB, using two wood screws
- Solder 4-pin row to step down converter. 
- With solder, shorten "5V" terminals on bottom side of step down converter. This sets-up converter to produce 5V output
- Put entire converter into connector board PCB
- Connect connector board PCB to RPi. Put it on the proper pins, otherwise something is gonna fry.


# Install software

Tortuguita's brain RPi uses SD card as a "hard drive" 
for operating system, control software and data.

You can use operating system of your choice, as long as it supports RPi hardware. 
Recommended OS is Raspberry Pi OS (formerly Raspbian OS)

As for the control software, Tortuguita uses opensource lightweight webserver based on Python and Flask, 
controlled over the Wi-Fi. 

Setting up both operating system and uploading software is straightforward process.
Please follow the instructions in [Tortuguita brain project](https://gitlab.com/tortuguita/brain).


# Final assembly  

Bolt everything together and connect the wires properly.
Be extremely careful when connecting the wires. Wrong wiring can easily destroy the electronics.    

## Assembly mechanics 
- Screw the sprocket assemblies to the chassis, using 20 mm screws
- Screw all road wheel assemblies to the chassis
- Put the tracks on
- Bolt idler to the chassis. Adjust position so the tracks are not too tight
- Screw rear connector to the chassis
- Screw motors to the chassis, motor nut goes to bolt head. Make sure everything rotates freely and motors and sprockets are in axis. 

## Assembly electronics 
- Screw brain assembly together with connector bay to the chassis.
- Screw battery assembly to the chassis
- Screw power part to the chassis
- Connect the wires

## Finish hardware 
- Screw bottom cover to the chassis.
- Put the connector bay cover on. 

Time for a little celebration. Your Tortuguita is now complete and ready to run.


# Charging

Tortuguita is charged over the 3.5 mm jack connector. 
Most convenient way is to make use of power cable (see instructions above) to charge it from car lighter socket.

Another option is to use external power source in range 11-15 V. Thanks to internal electronics, 
Tortuguita is quite tolerant to variations in charge voltage. 


# First ride 

Power on Tortuguita, using main switch.
Wait 1-2 minutes until she boots up.

Use ```nmap``` to find Tortuguita in your local network.
Hint: port 5000 is open :)

    nmap <your local segment> -p5000
    # example output: 
    # 192.168.0.15

*Tip: The IP address can change between reboots, depending on your router configuration. 
But almost every router has a function of assigning static IP address to a device (MAC address).* 

Now open Tortuguita's IP address in the browser, port 5000:

    http://192.168.0.15:5000

You should see main Tortuguita screen and live camera stream. 
To move, drag finger or mouse over image. 


# Useful links

https://gitlab.com/tortuguita/brain  : repository with software  
https://gitlab.com/tortuguita/body  : repository with hardware  

https://tortuguita.tech  : Homepage, shop with Tortuguita kits  
